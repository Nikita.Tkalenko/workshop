<?php

namespace AppBundle\Entity;

class User
{
    const ADULT_AGE = 21;

    /** @var int */
    private $age;

    /** @var Country $country */
    private $country;

    /**
     * @param int $age
     * @return User
     */
    public function setAge(int $age): User
    {
        if ($age < 0) {
            throw new \InvalidArgumentException("Age should can not be negative");
        }

        $this->age = $age;

        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return bool
     */
    public function isAdult(): bool
    {
        return $this->getAge() >= $this->getCountry()->getAdultAge();
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return User
     */
    public function setCountry(Country $country): User
    {
        $this->country = $country;

        return $this;
    }
}
