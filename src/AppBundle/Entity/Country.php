<?php

namespace AppBundle\Entity;

class Country
{
    /** @var int */
    private $adultAge;

    /**
     * @return int
     */
    public function getAdultAge(): int
    {
        return $this->adultAge;
    }
}
