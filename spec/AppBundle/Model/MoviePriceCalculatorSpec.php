<?php

namespace spec\AppBundle\Model;

use AppBundle\Entity\Movie;
use AppBundle\Entity\User;
use AppBundle\Model\MoviePriceCalculator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MoviePriceCalculatorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(MoviePriceCalculator::class);
    }

    function it_calculates_full_price_for_adult(
        User $user,
        Movie $movie
    ) {
        $user->isAdult()->willReturn(true);
        $movie->getPrice()->willReturn(20);

        ($this->calculatePrice($user, $movie))->shouldBe(20);
    }

    function it_calculates_half_price_for_kid(
        User $user,
        Movie $movie
    ) {
        $user->isAdult()->willReturn(false);
        $movie->getPrice()->willReturn(20);

        ($this->calculatePrice($user, $movie))->shouldBe(10);
    }
}
