<?php

namespace spec\AppBundle\Entity;

use AppBundle\Entity\Country;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CountrySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Country::class);
    }
}
