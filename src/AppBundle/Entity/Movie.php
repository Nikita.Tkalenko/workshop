<?php

namespace AppBundle\Entity;

class Movie
{
    /** @var string */
    private $name;

    /** @var int */
    private $price;

    /**
     * @param int $price
     * @return Movie
     */
    public function setPrice(int $price): Movie
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param string $name
     * @return Movie
     */
    public function setName(string $name): Movie
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
