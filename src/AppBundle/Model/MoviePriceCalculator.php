<?php

namespace AppBundle\Model;

use AppBundle\Entity\Movie;
use AppBundle\Entity\User;

class MoviePriceCalculator
{
    public function calculatePrice(User $user, Movie $movie)
    {
        if (!$user->isAdult()) {
            return $movie->getPrice() / 2;
        }

        return $movie->getPrice();
    }
}
