<?php

namespace spec\AppBundle\Entity;

use AppBundle\Entity\Country;
use AppBundle\Entity\User;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(User::class);
    }

    function its_is_adult_returns_true_for_people_over_21(Country $country)
    {
        $this->setAge(35);
        $this->setCountry($country);

        $country->getAdultAge()->willReturn(21);

        $this->isAdult()->shouldBe(true);
    }

    function its_is_adult_returns_false_for_people_under_21(Country $country)
    {
        $this->setAge(20);
        $this->setCountry($country);

        $country->getAdultAge()->willReturn(21);

        $this->isAdult()->shouldBe(false);
    }

    function its_set_age_throws_invalid_argument_exception_for_negative_number(User $user)
    {
        $this->shouldThrow(\InvalidArgumentException::class)->duringSetAge(-5);
    }
}
