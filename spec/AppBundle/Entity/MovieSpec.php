<?php

namespace spec\AppBundle\Entity;

use AppBundle\Entity\Movie;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MovieSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Movie::class);
    }
}
